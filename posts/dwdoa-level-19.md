---
title: Mission 19 Blood Money
description: ⏱ Speedrun Fast Playthrough Mission 19 Blood Money
date: 2018-07-04
tags: desperados wanted dead or alive
layout: layouts/post.njk
---
## Desperados Wanted Dead or Alive
<h3>⏱ Speedrun Mission 19: Blood Money</h3>
<div class="videoWrapper">
<iframe width="560" height="315" src="https://www.youtube.com/embed/51Bxdjw4ZS0?start=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<ul class="pagination modal-4">
<li><a href="{{ '/posts/dwdoa-level-18/' | url }}" class="prev">Previous</a></li>
<li><a href="{{ '/posts/dwdoa-level-20/' | url }}" class="next">Next</a></li>
</ul>