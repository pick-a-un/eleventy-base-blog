---
title: Mission 01 An Old Friend
description: This is a post on My Blog about leveraging agile frameworks
date: 2018-07-04
tags: desperados wanted dead or alive
layout: layouts/post.njk
---
## Mission 01: An Old Friend

<div class="videoWrapper">
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/IYK5kuRsFY8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>  

<ul class="pagination modal-4">
<li><a href="{{ '#' | url }}" class="prev">Previous</a></li>
<li><a href="{{ '/posts/dwdoa-level-02/' | url }}" class="next">Next</a></li>
</ul>