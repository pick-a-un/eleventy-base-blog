---
title: Mission 15 Dance with the Devil
description: ⏱ Speedrun Fast Playthrough Mission 15 Dance with the Devil
date: 2018-07-04
tags: desperados wanted dead or alive
layout: layouts/post.njk
---
## Desperados Wanted Dead or Alive
<h3>⏱ Speedrun Mission 15: Dance with the Devil</h3>
<div class="videoWrapper">
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/r1SKYkKCzgA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<ul class="pagination modal-4">
<li><a href="{{ '/posts/dwdoa-level-14/' | url }}" class="prev">Previous</a></li>
<li><a href="{{ '/posts/dwdoa-level-16/' | url }}" class="next">Next</a></li>
</ul>