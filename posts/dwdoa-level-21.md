---
title: Mission 21 Piggies in the Middle
description: ⏱ Speedrun Fast Playthrough Mission 21 Piggies in the Middle
date: 2018-07-04
tags: desperados wanted dead or alive
layout: layouts/post.njk
---
## Desperados Wanted Dead or Alive
<h3>⏱ Speedrun Mission 21: Piggies in the Middle</h3>
<div class="videoWrapper">
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/9ed2uBqGAk0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<ul class="pagination modal-4">
<li><a href="{{ '/posts/dwdoa-level-20/' | url }}" class="prev">Previous</a></li>
<li><a href="{{ '/posts/dwdoa-level-22/' | url }}" class="next">Next</a></li>
</ul>