---
title: Mission 03 Smoke Signals
description: ⏱ Speedrun Fast Playthrough Mission 3 Smoke Signals
date: 2018-07-04
tags: desperados wanted dead or alive
layout: layouts/post.njk
---
## Mission 03: Smoke Signals

<div class="videoWrapper">
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/u77zb-FRStE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<ul class="pagination modal-4">
<li><a href="{{ '/posts/dwdoa-level-02/' | url }}" class="prev">Previous</a></li>
<li><a href="{{ '/posts/dwdoa-level-04/' | url }}" class="next">Next</a></li>
</ul>