---
title: Mission 24 Inferno
description: ⏱ Speedrun Fast Playthrough Mission 24 Inferno
date: 2018-07-04
tags: desperados wanted dead or alive
layout: layouts/post.njk
---
## Desperados Wanted Dead or Alive
<h3>⏱ Speedrun Mission 24: Inferno</h3>
<div class="videoWrapper">
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/sWKFpLHvd7w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<ul class="pagination modal-4">
<li><a href="{{ '/posts/dwdoa-level-23/' | url }}" class="prev">Previous</a></li>
<li><a href="{{ '/posts/dwdoa-level-24/#' | url }}" class="next">Next</a></li>
</ul>