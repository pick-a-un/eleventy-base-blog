---
title: Mission 23 At the Gates of Hell
description: ⏱ Speedrun Fast Playthrough Mission 23 At the Gates of Hell
date: 2018-07-04
tags: desperados wanted dead or alive
layout: layouts/post.njk
---
## Desperados Wanted Dead or Alive
<h3>⏱ Speedrun Mission 23: At the Gates of Hell</h3>
<div class="videoWrapper">
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/dgmp6IMvRos" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<ul class="pagination modal-4">
<li><a href="{{ '/posts/dwdoa-level-22/' | url }}" class="prev">Previous</a></li>
<li><a href="{{ '/posts/dwdoa-level-24/' | url }}" class="next">Next</a></li>
</ul>