---
title: Mission 18 The Magnificent Six
description: ⏱ Speedrun Fast Playthrough Mission 18 The Magnificent Six
date: 2018-07-04
tags: desperados wanted dead or alive
layout: layouts/post.njk
---
## Desperados Wanted Dead or Alive
<h3>⏱ Speedrun Mission 18: The Magnificent Six</h3>
<div class="videoWrapper">
<iframe width="560" height="315" src="https://www.youtube.com/embed/9ed2uBqGAk0?start=3" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<ul class="pagination modal-4">
<li><a href="{{ '/posts/dwdoa-level-17/' | url }}" class="prev">Previous</a></li>
<li><a href="{{ '/posts/dwdoa-level-19/' | url }}" class="next">Next</a></li>
</ul>