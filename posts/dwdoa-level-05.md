---
title: Mission 05 Doc McCoy's Cabin
description: ⏱ Speedrun Fast Playthrough Mission 05 Doc McCoy's Cabin
date: 2018-07-04
tags: desperados wanted dead or alive
layout: layouts/post.njk
---
## Mission 05: Doc McCoy's Cabin

<div class="videoWrapper">
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/aISAXBhf4fM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<ul class="pagination modal-4">
<li><a href="{{ '/posts/dwdoa-level-04/' | url }}" class="prev">Previous</a></li>
<li><a href="{{ '/posts/dwdoa-level-06/' | url }}" class="next">Next</a></li>
</ul>
