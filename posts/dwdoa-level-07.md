---
title: Mission 07 A Woman's Weapons
description: ⏱ Speedrun Fast Playthrough Mission 07 A Woman's Weapons
date: 2018-07-04
tags: desperados wanted dead or alive
layout: layouts/post.njk
---
## Desperados Wanted Dead or Alive
<h3>⏱ Speedrun Mission 07: A Woman's Weapons</h3>

<div class="videoWrapper">
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/cwyAWKkgI0o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<ul class="pagination modal-4">
<li><a href="{{ '/posts/dwdoa-level-06/' | url }}" class="prev">Previous</a></li>
<li><a href="{{ '/posts/dwdoa-level-08/' | url }}" class="next">Next</a></li>
</ul>