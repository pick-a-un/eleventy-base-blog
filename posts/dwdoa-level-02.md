---
title: Mission 02 Southern Comfort
description: ⏱ Speedrun Fast Playthrough Mission 2 Southern Comfort
date: 2018-07-04
tags: desperados wanted dead or alive
layout: layouts/post.njk
---
## Desperados Wanted Dead or Alive - Mission 2: Southern Comfort

<div class="videoWrapper">
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/YQLzjo39aYA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<ul class="pagination modal-4">
<li><a href="{{ '/posts/dwdoa-level-01/' | url }}" class="prev">Previous</a></li>
<li><a href="{{ '/posts/dwdoa-level-03/' | url }}" class="next">Next</a></li>
</ul>