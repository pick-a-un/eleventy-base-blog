---
title: Mission 04 Hang'm High!
description: ⏱ Speedrun Fast Playthrough Mission 04 Hang'm High!
date: 2018-07-04
tags: desperados wanted dead or alive
layout: layouts/post.njk
---
## Mission 04: Hang'm High!

<div class="videoWrapper">
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/VXQiQ7S8vNI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<ul class="pagination modal-4">
<li><a href="{{ '/posts/dwdoa-level-03/' | url }}" class="prev">Previous</a></li>
<li><a href="{{ '/posts/dwdoa-level-05/' | url }}" class="next">Next</a></li>
</ul>
