---
layout: layouts/post.njk
title: About Me
templateClass: tmpl-post
eleventyNavigation:
  key: About Me
  order: 3
---

<p>I am a person doing stuff. Sometime that stuff is fluff.</p>
<br>
<strong>Do you want to support me?</strong>
<p>
<form style="display: inline" action="https://www.buymeacoffee.com/ynotyout" method="get" class="form-btn" target="_blank" rel="noopener noreferrer">
<button class="slide" type="submit" value="submit some bucks for a coffee">☕ Buy me some coffee</span></button>
</form>
</p>
<br>
<strong>Do you want to get it <span class="grab">touch</span>?</strong>
<p>Here you can drop me some lines</p>
<form name="contact" method="POST" data-netlify="true">
  <p>
    <label>Your Name: <input type="text" name="name" required/></label>   
  </p>
  <p>
    <label>Your Email: <input type="email" name="email" /></label>
  </p>
  <p>
    <label class="select">Your Role:<select name="role[]" id="slct">
      <option selected disabled>Choose an option</option>
      <option value="follower">Follower</option>
      <option value="leader">Leader</option>
      <option value="folder">Folder</option>
      <option value="lowerlea">lower Lea</option>
      <option value="none">None of the above</option>
    </select>
   </label>
  </p>
  <p class="vh">
    <label>Your enquiry<input name="bot-field" /></label>
  </p>
  <p>
    <label>Message: <textarea name="message" maxlength="512" required></textarea></label>
  </p>
  <p class="file">
        <input name="image" type="file" id="file" class="feedback-input">
  </p>
  <p>
    <button class="slide" type="submit" value="submit the form">Send me a message</button>
  </p>
</form>
